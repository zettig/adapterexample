package ru.igriv.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import ru.igriv.adapter.R;
import ru.igriv.adapter.model.SecondViewItem;

public class SecondItemViewHolder extends BaseViewHolder<SecondViewItem> {
    private TextView tv;
    public SecondItemViewHolder(@NonNull View itemView) {
        super(itemView);
        tv = itemView.findViewById(R.id.item_second_text);
    }

    @Override
    public void bind(SecondViewItem item) {
        tv.setText(item.getText());
    }
}
