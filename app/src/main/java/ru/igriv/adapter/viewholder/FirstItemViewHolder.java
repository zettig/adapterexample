package ru.igriv.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import ru.igriv.adapter.R;
import ru.igriv.adapter.model.FirstViewItem;

public class FirstItemViewHolder extends BaseViewHolder<FirstViewItem> {
    private TextView tv;
    public FirstItemViewHolder(@NonNull View itemView) {
        super(itemView);
        tv = itemView.findViewById(R.id.item_first_text);
    }

    @Override
    public void bind(FirstViewItem item) {
        tv.setText(item.getText());
    }
}
