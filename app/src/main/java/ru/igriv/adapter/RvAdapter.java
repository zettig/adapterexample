package ru.igriv.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.igriv.adapter.model.BaseItem;
import ru.igriv.adapter.viewholder.BaseViewHolder;
import ru.igriv.adapter.viewholder.FirstItemViewHolder;
import ru.igriv.adapter.viewholder.SecondItemViewHolder;

public class RvAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<BaseItem> items;

    public RvAdapter(List<BaseItem> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getLayoutResId();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case R.layout.item_first:
                return new FirstItemViewHolder(view);
            case R.layout.item_second:
                return new SecondItemViewHolder(view);
        }
        throw new IllegalArgumentException("Unknown view type");
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
