package ru.igriv.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ru.igriv.adapter.model.BaseItem;
import ru.igriv.adapter.model.FirstViewItem;
import ru.igriv.adapter.model.SecondViewItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        RvAdapter adapter = new RvAdapter(getData());
        recyclerView.setAdapter(adapter);
    }


    private List<BaseItem> getData() {
        List<BaseItem> result = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            if (i % 3 == 0) {
                result.add(new SecondViewItem("Second item " +  i));
            } else {
                result.add(new FirstViewItem("First item " + i));
            }
        }
        return result;
    }
}
