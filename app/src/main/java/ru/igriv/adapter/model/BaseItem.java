package ru.igriv.adapter.model;

public abstract class BaseItem {
    public BaseItem(String text) {
        this.text = text;
    }

    private String text;
    public abstract int getLayoutResId();

    public String getText() {
        return text;
    }
}
