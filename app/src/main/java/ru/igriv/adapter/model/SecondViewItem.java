package ru.igriv.adapter.model;

import ru.igriv.adapter.R;

public class SecondViewItem extends BaseItem {
    public SecondViewItem(String text) {
        super(text);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_second;
    }
}
