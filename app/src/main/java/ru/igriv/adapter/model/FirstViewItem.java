package ru.igriv.adapter.model;

import ru.igriv.adapter.R;

public class FirstViewItem extends BaseItem {
    public FirstViewItem(String text) {
        super(text);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_first;
    }
}
